from __future__ import absolute_import, unicode_literals

import celery.signals

# noinspection PyUnresolvedReferences
from config import config  # Let it load the entire .env and stuff

broker_url = f'redis://{config.REDIS_CELERY["HOST"]}:{config.REDIS_CELERY["PORT"]}/{config.REDIS_CELERY["DB"]}'
result_backend = broker_url

# http://docs.celeryproject.org/en/latest/getting-started/brokers/redis.html#id1
broker_transport_options = {'visibility_timeout': 3600,
                            'fanout_prefix': True,
                            'fanout_patterns': True}

task_track_started = True
worker_hijack_root_logger = False

imports = ['app.event.tasks']


@celery.signals.setup_logging.connect
def stop_celery_logging_hijack(**kwargs):
    """Disrespects `worker_hijack_root_logger` (_even_ if you set it to `False`).
    That does nothing to not erase our existing logging conf, so we use this to tell celery to chill and call this
    function. Here we do a no-op so that our existing logging works"""
    import logging

    msg = 'DISABLED_LOGGING IGNORE_IF_DUPLICATE'
    print(msg)
    logging.info(msg)
