from __future__ import absolute_import, unicode_literals

import time

from app.auth.models import Token
from app.core.db.transaction import atomic
from app.core.utils import get_uuid
from app.extensions import db


def test_basic_assert():
    assert True is True


def test_basic_sql():
    db.session.execute('SELECT 1')


def test_basic_session_commit():
    j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.commit()


def test_basic_session_double_commit():
    j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.commit()

    j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
    db.session.add(j)
    db.session.flush()
    db.session.commit()


def test_basic_session_rollback():
    j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
    db.session.add(j)
    db.session.flush()
    rows = db.session.query(Token).all()
    assert len(rows) == 1
    db.session.rollback()


def test_basic_session_nested_commit():
    with atomic():
        j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
        db.session.add(j)

    rows = db.session.query(Token).all()
    assert len(rows) == 1

    db.session.commit()


def test_basic_session_nested_rollback():
    with atomic():
        j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
        db.session.add(j)

    rows = db.session.query(Token).all()
    assert len(rows) == 1

    db.session.rollback()


def test_basic_session_nested_failure():
    try:
        with atomic():
            j = Token(access_token=get_uuid(), expiry_timestamp=int(time.time()))
            db.session.add(j)
            raise RuntimeError
    except:
        pass

    rows = db.session.query(Token).all()
    assert len(rows) == 0

    db.session.commit()


def test_session_is_isolated():
    # TODO set up a way to put pre-filled data in case this test is run first or use pytest.mark hooks to run this last
    rows = db.session.query(Token).all()
    assert len(rows) == 0
