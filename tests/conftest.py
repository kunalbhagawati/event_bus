from __future__ import absolute_import, unicode_literals

import pytest

from app.app import create_app
from app.core.db.utils import create_schemas
from app.extensions import db


@pytest.fixture(scope='session')
def app():
    _app = create_app()
    return _app


@pytest.fixture(scope='session', autouse=True)
def setup_db(app):
    with app.app_context():
        # Check to see if DB online
        # SqlAlchemy does not create schema so we do it
        try:
            db.session.execute("SELECT 1")
        except Exception as e:
            pytest.exit(f'COULD_NOT_CONNECT_TO_DB | exc: {e}')

        create_schemas()
        db.create_all()


@pytest.fixture(autouse=True)
def _wrap_nested():
    db.session.begin(nested=True)

    # then each time that SAVEPOINT ends, reopen it
    @db.event.listens_for(db.session, "after_transaction_end")
    def restart_savepoint(session, transaction):
        if transaction.nested and not transaction._parent.nested:
            # ensure that state is expired the way
            # session.commit() at the top level normally does
            # (optional step)
            session.expire_all()

            session.begin_nested()

    yield

    db.session.rollback()
    db.session.remove()
