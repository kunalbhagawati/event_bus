"""
File to expose the app so that it is run directly by the UWSGI server.
nginx cannot understand the appplication fctory (`create_app`) that we use directly
(via the flask command line in dev and py.test mode).
This is the app bundled and packaged to nginx directly.
"""
import os

from app.app import create_app

app = create_app()


if __name__ == '__main__':
    app.run(port=os.environ['FLASK_RUN_PORT'])
