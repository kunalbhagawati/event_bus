from __future__ import absolute_import, unicode_literals

from enum import Enum


class UserRoleEnum(str, Enum):
    ADMIN = 'ADMIN'


MAX_TOKEN_VALIDITY_HOURS = 24 * 30  # 1 month approx
