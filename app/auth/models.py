from __future__ import absolute_import, unicode_literals

from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy_utils import PasswordType

from app.auth.constants import UserRoleEnum
from app.core.db.fields import ArrowTimestamp
from app.core.db.mixins import SoftDeletesMixin, TrackedModificationsMixin
from app.extensions import db


def get_event_cls():
    # To avoid circualr imports
    from app.event.models import Event
    return Event


class User(UserMixin, SoftDeletesMixin, TrackedModificationsMixin, db.Model):
    __tablename__ = 'users'

    name = db.Column(db.Text, nullable=False)
    username = db.Column(db.Text, nullable=False, unique=True)
    role = db.Column(db.Enum(UserRoleEnum, native_enum=False, create_constraint=False), nullable=True)
    password = db.Column(PasswordType(schemes=['pbkdf2_sha512']), nullable=False)

    # retuns a query object instead of a collection. This query can be further wrangled.
    query__logins = db.dynamic_loader(lambda: Token, backref='user')


class Token(TrackedModificationsMixin, db.Model):
    __tablename__ = 'token'

    user_id = db.Column(db.Integer, db.ForeignKey(User.__table__.c.id), nullable=False)
    access_token = db.Column(UUID, nullable=False, unique=True)
    expiry_timestamp = db.Column(ArrowTimestamp, nullable=False)
