from __future__ import absolute_import, unicode_literals

from flask_api import status
from flask_classful import FlaskView
from sqlalchemy.orm.exc import NoResultFound

from app.auth.interactors import login_user
from app.auth.models import User
from app.auth.schemas import TokenSchema
from app.core.exceptions import Exc
from app.core.flaskapiparser import parser
from app.extensions import db, ma


# Schemas
class LoginArgsSchema(ma.Schema):
    username = ma.String(required=True)
    password = ma.String(required=True)

    class Meta:
        strict = True


# Views
class LoginView(FlaskView):
    def post(self):
        """
        # Payload
        {
            "username": "admin",
            "password": "admin123"
        }
        """
        args = parser.parse(LoginArgsSchema())
        try:
            user = db.session.query(User) \
                .filter(User.username == args['username'].strip()) \
                .one()
        except NoResultFound:
            return {}, status.HTTP_404_NOT_FOUND

        if user.password != args['password']:
            raise Exc('INCORRECT_PASSWORD', 'The password does not match', user_id=user.id)

        token = login_user(user.id)
        db.session.commit()
        return {'data': TokenSchema(only=['access_token', 'user_id', 'expiry_timestamp']).dump(token)}, \
               status.HTTP_200_OK


def register(app):
    LoginView.register(app, route_base='login')
