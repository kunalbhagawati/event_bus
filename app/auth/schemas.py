from __future__ import absolute_import, unicode_literals

from app.auth.models import Token
from app.core.serializers import ArrowField
from app.extensions import ma


class TokenSchema(ma.ModelSchema):
    user_id = ma.Integer()
    expiry_timestamp = ArrowField()
    entry_timestamp = ArrowField()

    class Meta:
        model = Token
