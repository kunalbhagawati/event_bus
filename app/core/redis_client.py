from __future__ import absolute_import, unicode_literals

from redis import StrictRedis


def get_client() -> StrictRedis:
    """Gives back a StrictRedis client.
    NOTE If you create an instance in the module level, it _will_ load the config.
    It's not harmful per se, but make sure that this is what you want.
    """

    # import here to avoid loading the .env since that is the common convention across the codebase
    from config import config

    return StrictRedis(host=config.REDIS["HOST"], port=config.REDIS["PORT"], db=config.REDIS["DB"])
