from __future__ import absolute_import, unicode_literals

from webargs import core
from webargs.flaskparser import FlaskParser


class _FlaskParser(FlaskParser):
    """Handles FlaskAPIs disrespect of the request json datastream"""

    def parse_json(self, req, name, field):
        """Bypass the usual json location to instead look at request.data since FlaskAPI changes the json data stream to
        `data` and that is not available anymore. Otherwise remains the same as the parent class method"""
        json_data = req.data
        if json_data is None:
            return core.missing
        return core.get_value(json_data, name, field, allow_many_nested=True)


parser = _FlaskParser()
use_args = parser.use_args
use_kwargs = parser.use_kwargs
