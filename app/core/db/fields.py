from __future__ import absolute_import, unicode_literals

import arrow

from app.extensions import db


class ArrowTimestamp(db.TypeDecorator):
    """Internally an integer object"""

    impl = db.INTEGER

    def process_bind_param(self, value, dialect):
        if not value:
            return None

        if isinstance(value, arrow.Arrow):
            return value.timestamp
        if isinstance(value, (int, str)):
            return int(value)

        raise NotImplementedError

    def process_result_value(self, value, dialect):
        if value is None:
            return None

        return arrow.get(value).to('Asia/Kolkata')
