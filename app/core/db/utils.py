from __future__ import absolute_import, unicode_literals

from sqlalchemy.schema import DropSchema

from app.extensions import db

SCHEMA_NAME = 'public'


def create_schemas():
    """This assumes that the db is initialized with the app. i.e there is an app context"""
    # Not using the CreateSchema since that does not do a check for IF NOT EXISTS
    db.engine.execute(db.DDL(f'CREATE SCHEMA IF NOT EXISTS {SCHEMA_NAME}'))


def drop_schemas():
    db.engine.execute(DropSchema(SCHEMA_NAME))
