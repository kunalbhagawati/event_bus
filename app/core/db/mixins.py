from __future__ import absolute_import, unicode_literals

import logging
import time

from flask import g

from app.core.db.fields import ArrowTimestamp
from app.extensions import db

logger = logging.getLogger()


def get_current_timestamp(context) -> int:
    return int(time.time())


class EntryTimestampMixin(object):
    entry_timestamp = db.Column(ArrowTimestamp, default=get_current_timestamp, nullable=False)


class ModifiedTimestampMixin(object):
    modified_timestamp = db.Column(ArrowTimestamp,
                                   default=get_current_timestamp,
                                   onupdate=get_current_timestamp,
                                   nullable=False)


def get_internal_user(context):
    if context.current_parameters.get('modified_user_id'):
        return context.current_parameters['modified_user_id']
    logger.warning('This mode of automatically putting the user from the global `g` object is marked for deprecation')
    return g.current_user.id


class TrackedEntryUserMixin(EntryTimestampMixin):
    """
    For setting entry_timestamp, and entry_user_id automagically
    """
    entry_user_id = db.Column(db.Integer, default=get_internal_user)


class TrackedModificationsMixin(TrackedEntryUserMixin, ModifiedTimestampMixin):
    modified_user_id = db.Column(db.Integer,
                                 default=get_internal_user,
                                 onupdate=get_internal_user,
                                 nullable=False)


class SoftDeletesMixin(object):
    """
    Sets a soft deleted column and excludes those rows.
    To get the rows with the results use `_with_deleted=True` in the .query() part.
    """

    __soft_deletes__ = True

    # http://stackoverflow.com/questions/23198801/sqlalchemy-using-aliased-in-query-with-custom-primaryjoin-relationship
    db_deleted_timestamp = db.Column(ArrowTimestamp)
