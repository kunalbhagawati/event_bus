from __future__ import absolute_import, unicode_literals

from contextlib import contextmanager

from app.extensions import db


@contextmanager
def attempt_commit():
    """
    Puts a commit at the end of the function block.
    Meant to be used with top level functions such as views, interactors, etc.
    On any uncaught exception, will issue a rollback.
    """

    yield
    db.session.commit()


@contextmanager
def atomic():
    """
    Opens up a database transaction, and commits/rollsback in the end.
    """
    db.session.begin(nested=True)

    try:
        yield
        db.session.commit()
    except:
        db.session.rollback()
        raise


@contextmanager
def flush():
    yield
    db.session.flush()
