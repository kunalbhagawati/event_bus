from __future__ import absolute_import, unicode_literals

import time

from flask_sqlalchemy import BaseQuery, SQLAlchemy


class SoftDeleteHandlingQuery(BaseQuery):
    def __init__(self, *args, **kwargs):
        self.with_deleted = kwargs.pop('_with_deleted', False)
        super(SoftDeleteHandlingQuery, self).__init__(*args, **kwargs)

    def get(self, ident):
        # override get() so that the flag is always checked in the
        # DB as opposed to pulling from the identity map. - this is optional.
        return BaseQuery.get(self.populate_existing(), ident)

    def __iter__(self):
        return BaseQuery.__iter__(self.undeleted())

    def _should_handle_soft_delete(self):
        if self.with_deleted:
            return False

        mzero = self._mapper_zero()
        if mzero is None or not hasattr(mzero, 'class_'):
            return False

        if not hasattr(mzero.class_, 'db_deleted_timestamp'):
            return False

        if getattr(mzero.class_, '__soft_deletes__', False):  # The only case this is True
            return True

        return False

    def from_self(self, *ent):
        # override from_self() to automatically apply
        # the criterion too.   this works with count() and
        # others.
        return BaseQuery.from_self(self.undeleted(), *ent)

    def update(self, *args, **kwargs):
        return BaseQuery.update(self.undeleted(), *args, **kwargs)

    def delete(self, synchronize_session='evaluate'):
        if not self._should_handle_soft_delete():
            return super(SoftDeleteHandlingQuery, self).delete(synchronize_session=synchronize_session)

        return BaseQuery.update(self.undeleted(),
                                {'db_deleted_timestamp': int(time.time())},
                                synchronize_session=synchronize_session)

    def undeleted(self):
        if not self._should_handle_soft_delete():
            return self
        mzero = self._mapper_zero()  # this is bound to exist as the above check fails if `not mzero`
        return self.enable_assertions(False).filter(mzero.class_.db_deleted_timestamp == None)


# NOTE not fully tested
def soft_delete_relationship(first, second, *args, **kwargs):
    if isinstance(first, str) and isinstance(second, str):
        other, other_column = first.split('.')
        primaryjoin = ' & '.join(['({} == {})'.format(first, second), '{}.db_deleted_timestamp == None'.format(other)])
    else:
        other, other_column = args[0].split('.')
        primaryjoin = lambda: (first == second) & getattr(second.table.c, 'db_deleted_timestamp') == None

    kwargs['primaryjoin'] = primaryjoin
    return SQLAlchemy.relationship(other, **kwargs)
