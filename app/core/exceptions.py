from __future__ import absolute_import, unicode_literals

from flask_api.exceptions import APIException


class Exc(APIException):
    """Exception with a class code, description and extra information as arguments"""

    status_code = 500

    def __init__(self, detail: str, desc: str = None, **kwargs):
        self.detail = detail
        self.extra = kwargs
        self.desc = desc
        super().__init__(str(detail))

    def __str__(self):
        extras_msg = '  '.join(['{}={}'.format(k, v) for k, v in self.extra.items()])
        return f'{self.__class__.__name__} <{self.detail}> | {extras_msg}'.strip()

    def __repr__(self):
        return str(self)
