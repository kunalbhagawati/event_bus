from __future__ import absolute_import, unicode_literals

import logging
from typing import Dict

from flask import g, current_app, has_app_context

from app.app import create_app
from app.auth.models import User
from app.extensions import db


class AppContext(object):
    """Initiates an app context. Uses the `current_app` or creates a new one."""

    def __init__(self, g_vars_mapping: Dict = None):
        self.g_vars_mapping = g_vars_mapping or {}
        self._pushed = False

    def _set_g_vals(self):
        for k, v in self.g_vars_mapping.items():
            setattr(g, k, v)

    def __enter__(self):

        if current_app:
            logging.info('FOUND_CURRENT_APP')
            app = current_app
        else:
            logging.info('CREATING_APP')
            app = create_app()

        self.ctx = app.app_context()
        if not has_app_context():
            logging.info('NO_APP_CONTEXT_FOUND PUSHING')
            self.ctx.push()
            self._pushed = True
        else:
            logging.debug('APP_CONTEXT_FOUND NOT_PUSHING')

        self._set_g_vals()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._pushed:
            logging.info('REMOVING_APP_CONTEXT')
            self.ctx.pop()
        else:
            logging.debug('PREV_APP_CONTEXT_PRESENT NOT_REMOVING')


class DefaultVarsAppContext(AppContext):
    """Puts a default `current_user` in g vars.
    NOTE overrides if already exists"""

    def _set_g_vals(self):
        admin = db.session.query(User).filter(User.username == 'admin').one()
        g_vars = {'current_user': admin}
        g_vars.update(self.g_vars_mapping)

        for k, v in g_vars.items():
            setattr(g, k, v)
