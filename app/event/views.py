from __future__ import absolute_import, unicode_literals

import logging
import time

import webargs
from flask import url_for, request
from flask_api import status
from flask_classful import FlaskView, route
from flask_login import login_required
from webargs import fields

from app.core.flaskapiparser import parser
from app.event.interactors import get_events, update_events
from app.event.schemas import EventSchema
from app.event.tasks import handle_event
from app.extensions import ma, db


# Schemas
class EventPatchSchema(ma.Schema):
    id = ma.Int(required=True)
    read_timestamp = ma.Float(required=True)

    class Meta:
        strict = True


class PatchPayload(ma.Schema):
    data = fields.Nested(EventPatchSchema(many=True), required=True)

    class Meta:
        strict = True


# Views
class EventView(FlaskView):
    def post(self):
        # bugs with marshamllow / webargs integration on unknown fields. reverting to lower level request parsing
        event_data = request.data
        sender_id = request.data.get('sender_id')
        if not sender_id:
            raise webargs.ValidationError('`sender_id` is not passed', status_code=422)

        try:
            sender_id = int(sender_id)
        except (TypeError, ValueError):
            raise webargs.ValidationError('`sender_id` must be an integer', status_code=422)

        event_data['sender_id'] = sender_id

        handle_event.delay(event_data, str(time.time()))
        logging.info(f'RECEIVED_EVENT | event_data: {event_data}')  # change to async data store if required later
        return {}, status.HTTP_202_ACCEPTED

    @login_required
    @route('/', methods=['PATCH'])
    def bulk_patch(self):
        """
        # Payload
        {
        	"data": [{"id": 1, "read_timestamp": 1}]
        }
        """
        patch_data = parser.parse(PatchPayload())['data']
        if not patch_data:
            return {}, status.HTTP_202_ACCEPTED
        update_events(patch_data)
        db.session.commit()
        return {}, status.HTTP_202_ACCEPTED

    @login_required
    def index(self):
        """
        # Payload
        {
            "sender_id": 1,
            "some": "data",
            "some_other_key": "val"
        }
        """
        sent_args = parser.parse({'sender_id': ma.List(ma.Integer(), required=False),
                                  'project_id': ma.List(ma.String(), required=False),
                                  'page': ma.Int(missing=1)})
        events = get_events(**sent_args)
        next_url = url_for('EventView:index', page=events.next_num) \
            if events.has_next else None
        prev_url = url_for('EventView:index', page=events.prev_num) \
            if events.has_prev else None

        return {'data': EventSchema().dump(events.items, many=True),
                'next': next_url,
                'prev': prev_url}, \
               status.HTTP_200_OK


def register(app):
    EventView.register(app, route_base='event')
