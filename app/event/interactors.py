from __future__ import absolute_import, unicode_literals

import logging
import time
from typing import Dict, Optional

from flask import g

from app.auth.constants import UserRoleEnum
from app.core.db.transaction import flush
from app.core.exceptions import Exc
from app.core.logging import mark_boundary
from app.event.constants import EventType, MAX_PAGE_SIZE_FOR_EVENTS_READ
from app.event.models import Event
from app.extensions import db

# Stores the event handlers for the type of event
EVENT_TYPE_POST_PROCESSOR_MAP = {}


def validate_event_data_basic(event_data: Dict):
    """Basic checks to see if the event data should even be processed."""
    return True


def get_event_type_from_event_data(event_data: Dict):
    return None  # for now assume generic event type.


def validate_event_data_event_type(event_data: Dict, event_type: Optional[EventType]):
    pass


def post_process_event(event: Event):
    """Call the post processors on the event based on its type"""

    for fn in EVENT_TYPE_POST_PROCESSOR_MAP.get(event.event_type, []):
        fn(event)


def store_event(sender_id: int, event_data: Dict, event_type: Optional[EventType], received_timestamp: str) -> Event:
    # keeping separate so that we can swap out the data store later
    e = Event(sender_id=sender_id, data=event_data, event_type=event_type, received_timestamp=received_timestamp)
    db.session.add(e)
    return e


@flush()
@mark_boundary("HANDLE_EVENT")
def handle_event(sender_id: int, event_data: Dict, received_timestamp: str):
    """Processes the event and stores it to the data store"""
    # Do a simple validation and then validation based on the event type. Since this is a dict with a small number of
    # keys, i'm not worrying about double iterations. The basic validation has the potential to save the effort of more
    # complex time consuming validations later

    logging.info(f"VAR_DUMP | "
                 f"sender_id: {sender_id}  event_data: {event_data}  received_timestamp: {received_timestamp}")

    validate_event_data_basic(event_data)
    event_type = get_event_type_from_event_data(event_data)
    logging.debug(f"VAR_DUMP | event_type: {event_type}")

    validate_event_data_event_type(event_data, event_type)

    event = store_event(sender_id, event_data, event_type, received_timestamp)
    post_process_event(event)


def get_events_for_user(event_id: int):
    return db.Session.query(Event).get(event_id)


def get_events(**kwargs):
    sender_ids = kwargs.pop('sender_id', None)
    project_ids = kwargs.pop('project_id', None)

    query = db.session.query(Event)

    if sender_ids:
        query = query.filter(Event.sender_id.in_(sender_ids))

    if project_ids:
        query = query.filter(Event.data['project_id'].in_(project_ids))

    return query.paginate(page=kwargs.get('page', 1), per_page=kwargs.get('per_page', MAX_PAGE_SIZE_FOR_EVENTS_READ))


@flush()
@mark_boundary('UPDATE_EVENTS')
def update_events(update_dict: Dict):
    # Not using `get_events` here since that is put only for the view, and this layer is
    # anyway coupled enough to the data layer

    event_ids = []
    read_timestamps = []
    invalid = []
    event_id_updates_map = {}
    for i in update_dict:
        event_id = i.pop('id')  # removing the id key here
        event_ids.append(event_id)
        event_id_updates_map[event_id] = i

        read_ts = i['read_timestamp']
        if read_ts > time.time():
            invalid.append(i)

        read_timestamps.append(read_ts)

    if invalid:
        raise Exc("INVALID_TIMESTAMPS",
                  'Some of the timestamps are invalid',
                  invalid=invalid,
                  humor='Time machine not invented yet!')

    query = db.session.query(Event).filter(Event.id.in_(event_ids))
    if g.current_user.role != UserRoleEnum.ADMIN:
        query = query.filter(Event.sender_id == g.current_user.id)

    events = query.all()

    event_ids_not_matching = set(event_ids) - {i.id for i in events}

    if event_ids_not_matching:
        raise Exc('EVENT_ID_MISMATCH',
                  'Some of the event ids are not patchable',
                  event_ids_not_matching=event_ids_not_matching)

    for e in events:
        for update_key, update_val in event_id_updates_map.get(e.id).items():
            setattr(e, update_key, update_val)
        db.session.add(e)
