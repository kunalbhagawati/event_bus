from __future__ import absolute_import, unicode_literals

from app.core.serializers import ArrowField
from app.event.models import Event
from app.extensions import ma


class EventSchema(ma.ModelSchema):
    entry_timestamp = ArrowField()

    class Meta:
        model = Event
