from __future__ import absolute_import, unicode_literals

from sqlalchemy.dialects.postgresql import JSONB

from app.core.db.mixins import EntryTimestampMixin
from app.event.constants import EventType
from app.extensions import db


class Event(EntryTimestampMixin, db.Model):  # change to TrackedEntryUserMixin when auth is enabled
    # Temporary model till I refresh on MongoDB

    sender_id = db.Column(db.Integer, nullable=False, index=True)
    data = db.Column(JSONB, nullable=False)
    event_type = db.Column(db.Enum(EventType, native_enum=False, create_constraint=False), nullable=True)
    # store upto year 5138. Index for performance
    received_timestamp = db.Column(db.Numeric(precision=18, scale=6), nullable=False, index=True)
    read_timestamp = db.Column(db.Numeric(precision=18, scale=6), nullable=True)  # store upto year 5138
    # NOTE add metadata later if required
