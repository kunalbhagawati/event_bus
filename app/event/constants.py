from __future__ import absolute_import, unicode_literals

from enum import Enum


class EventType(str, Enum):
    SLACK = 'SLACK'
    JIRA = 'JIRA'


MAX_PAGE_SIZE_FOR_EVENTS_READ = 100
