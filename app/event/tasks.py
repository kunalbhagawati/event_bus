from __future__ import absolute_import, unicode_literals

from typing import Dict

from celery.task import task

from app.core.app import AppContext
from app.extensions import db
from .interactors import handle_event as handle_event_interactor


@task()
def handle_event(event_data: Dict, received_timestamp: str):
    """Thin wrapper to separate the celery task from the main logic.
    Just acts as a handshake since we want out celery imports to not be in the interactors.
    Allows for retry, etc later"""

    with AppContext():
        # be aware, dict is mutated on `pop`
        handle_event_interactor(event_data.pop('sender_id'), event_data, received_timestamp)
        db.session.commit()
