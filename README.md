# External Dependencies
- Python 3.6
- Postgres@9.6
- Redis (for celery)

# How to run
- Install the dependencies (I would suggest in a virtual environment)
    `pip install -r requirements/dev.pip`
- Create a .env file to hold the config
    ```
    # Required by flask to set up. Cannot set after code
    # http://flask.pocoo.org/docs/1.0/config/#environment-and-debug-features
    # This will be overwritten 
    FLASK_ENV=development
    FLASK_RUN_PORT=5003
    
    # DB
    EVENT_BUS__DB__DRIVER=postgres
    EVENT_BUS__DB__USERNAME=projects
    EVENT_BUS__DB__PASSWORD=projects
    EVENT_BUS__DB__HOST=localhost
    EVENT_BUS__DB__NAME=projects__event_bus
    EVENT_BUS__DB__PORT=5432
    
    # Celery
    EVENT_BUS__REDIS_CELERY_DB=7
    
    # Redis
    EVENT_BUS__REDIS_HOST=localhost
    EVENT_BUS__REDIS_DB=3
    EVENT_BUS__REDIS_PORT=6379
    
    # Testing
    EVENT_BUS__TESTING=0
    
    # Logging
    EVENT_BUS__LOGGING_FORMATTER='simple'
    ```
- Open the shell
    `FLASK_APP=app.app FLASK_ENV=development FLASK_DEBUG=1 flask shell`
    - Create the tables
        ```python
        from app.app import create_app
        app = create_app()
        app.app_context().push()
  
        from app.extensions import db
        db.create_all()
        ```
- Run the server `FLASK_APP=app.app FLASK_ENV=development FLASK_DEBUG=1 flask run`

# Indexes
While the ORM creates indexes on the columns, but some specific indexes are required 
(assuming your search path on creating the tables was set to `public` schema):
- `CREATE INDEX event_data_project_id_index ON public.event ((data->>'project_id'));` 

# App config
The app takes a default config from the `.env` file. To extend on this, you can pass a env variable 
`EVENT_BUS__CONFIG_NAME` which will then try to load config from the file `.env.<value>`. 
I use this in local testing where I set the env value to test and put a file called `.env.test` to override the default 
config. (refer `config.py`)

# API Auth
- Auth tokens must be passed via the `Authorization` header
e.g
```bash
curl -X PATCH \
  http://localhost:5003/event/ \
  -H 'Authorization: af5ad710-6abd-4b3b-bf61-e39ab2e77def' \
  -H 'Content-Type: application/json' \
  -d '{
	"data": [{"id": 1, "read_timestamp": 1}]
}'
```

# TODO 
- Move to MongoDB
- Test cases
- Better use for schemas (API validation and serialization both)
- Get hashed password from frontend

# Bugs
- Relationship loading for user and event models (right now buggy in ORM, thus disabled)
- Pagination url must take in query params

# Things to do
- Sentry/Rollbar for error reporting
- Swagger docs format
- Auto Migrations via `alembic` 

# Future extensibility
- Allow OTP based login
    - Unique key of username/phone_number
- If we continue with postgres then better archiving and sharding is needed to remove old data baggage on the CRUD 
of the table.
- Allow clients to give event metadata so that we can prioritize queues accordingly.
- Implement the signal handlers as listeners on the event save, so that is decoupled, and more generic to whichever 
service wants to handle it.

# Dummy data
```sql
INSERT INTO public.users (id, entry_timestamp, modified_timestamp, entry_user_id, modified_user_id, db_deleted_timestamp, name, role, password, username) VALUES (1, 1532292850, 1532292850, -1, -1, null, 'admin', 'ADMIN', '$pbkdf2-sha512$25000$/T.HcO49Z2xtbY0RQihFaA$dztEbDpCRZg//EZTHL9JMVQ3OqoN8Le7I6Tg/vS4wxsWgjfiLU0xk01.waUtsNcXVlxQnWhm1JvZyh/eeJw5lg', 'admin');
INSERT INTO public.users (id, entry_timestamp, modified_timestamp, entry_user_id, modified_user_id, db_deleted_timestamp, name, role, password, username) VALUES (3, 1532302652, 1532302652, 1, 1, null, 'Kunal', null, '$pbkdf2-sha512$25000$GoPwvjfGWEup9b6XMgZgzA$rP4P4YkUeExOODg.SbJ1OviL9aj.bMuYlyXEn.NHOtd27lxnMKsaWDh2iXh8qO56T1XDbU8snVqgjMeGehUbPg', 'kunal');

INSERT INTO public.token (id, entry_timestamp, modified_timestamp, entry_user_id, modified_user_id, user_id, access_token, expiry_timestamp) VALUES (3, 1532295907, 1532295907, 1, 1, 1, 'af5ad710-6abd-4b3b-bf61-e39ab2e77ded', 1534887907);
INSERT INTO public.token (id, entry_timestamp, modified_timestamp, entry_user_id, modified_user_id, user_id, access_token, expiry_timestamp) VALUES (5, 1532295907, 1532295907, 3, 3, 3, 'af5ad710-6abd-4b3b-bf61-e39ab2e77def', 1534887907);

INSERT INTO public.event (id, entry_timestamp, sender_id, data, event_type, received_timestamp, read_timestamp) VALUES (10, 1532300176, 1, '{"some": "thing", "project_id": 1}', null, 1532300176.134962, null);
INSERT INTO public.event (id, entry_timestamp, sender_id, data, event_type, received_timestamp, read_timestamp) VALUES (11, 1532300181, 1, '{"some": "thing", "project_id": 2}', null, 1532300181.827484, null);
INSERT INTO public.event (id, entry_timestamp, sender_id, data, event_type, received_timestamp, read_timestamp) VALUES (1, 1532300136, 1, '{"abc": 1, "some": "data"}', null, 1532300136.084760, 1.000000);
```
